#ifndef MESH_H
#define MESH_H

struct Mesh {
    std::vector <Vec3> vertices; //array of mesh vertices positions
    std::vector <Vec3> normals; //array of vertices normals useful for the display
    std::vector <Triangle> triangles; //array of mesh triangles
    std::vector <Vec3> triangle_normals; //triangle normals to display face normals

    Vec3 get_normal(Vec3 x0, Vec3 x1, Vec3 x2)
    {
        Vec3 v0 = x0 - x2;
        Vec3 v1 = x1 - x2;
        Vec3 n = Vec3::cross(v0, v1);

        n.normalize();
        return n;
    }

    //Compute face normals for the display
    void computeTrianglesNormals() {
        
        // Vider le vecteur triangle_normals (i.e. faire un clear du vecteur)
        //TODO: implémenter le calcul des normales par face
        //Iterer sur les triangles du maillage

            //La normal du triangle i est le resultat du produit vectoriel de deux ses arêtes e_10 et e_20 normalisé (e_10^e_20)
            //L'arete e_10 est représentée par le vecteur partant du sommet 0 (triangles[i][0]) au sommet 1 (triangles[i][1])
            //L'arete e_20 est représentée par le vecteur partant du sommet 0 (triangles[i][0]) au sommet 2 (triangles[i][2])

            //Normaliser le resultat, utiliser la fonction normalize()

            //Ajouter dans triangle_normales

        triangle_normals.clear();
        //triangle_normals.resize(triangles.size());

        for(int f = 0; f < triangles.size();f++)
        {
            Vec3 normal = get_normal(vertices[triangles[f][0]],vertices[triangles[f][1]],vertices[triangles[f][2]]);
            triangle_normals.push_back(normal);
        }
    }

    //Compute vertices normals as the average of its incident faces normals
    void computeVerticesNormals() {
        
        normals.clear();
        normals.resize(vertices.size());

        for(int v = 0; v < vertices.size();v++)
        {
            normals[v] = Vec3(0,0,0);
        }

        for(int f = 0; f < triangles.size();f++)
        {
            normals[triangles[f][0]] +=  triangle_normals[f];
            normals[triangles[f][1]] +=  triangle_normals[f];
            normals[triangles[f][2]] +=  triangle_normals[f];  
        }

        for(int v = 0; v < normals.size();v++)
        {
            normals[v].normalize();
        }
    }

    void computeNormals() {
        computeTrianglesNormals();
        computeVerticesNormals();
    }

    Mesh(){}

    Mesh( Mesh const& i_mesh):
        vertices(i_mesh.vertices),
        normals(i_mesh.normals),
        triangles(i_mesh.triangles),
        triangle_normals(i_mesh.triangle_normals)
    {}

    Mesh( std::vector <Vec3> const& i_vertices, std::vector <Triangle> const& i_triangles):
        vertices(i_vertices),
        triangles(i_triangles)
    {
        computeNormals();
    }

};


#endif // MESH_H
